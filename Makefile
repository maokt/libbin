all: l b

clean:
	rm b l in.o

in.o: in.c
	$(CC) -c in.c -o in.o

b: b.c l in.o
	$(CC) -Wall -fpic -shared $^ in.o -l:l -o $@ -Wl,-e,start

l: l.c in.o
	$(CC) -Wall -fpic -shared $^ in.o -o $@ -Wl,-e,start

