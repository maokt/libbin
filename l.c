#include <stdlib.h>
#include <stdio.h>

static void debug(char *msg) {
    puts(msg);
}

void say(char *msg) {
    debug("say");
    puts(msg);
}

void start(void) {
    debug("This is l start");
    say("hello from l");
    exit(0);
}

